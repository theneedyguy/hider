olefile==0.46
Pillow==6.2.0
pycrypto==2.6.1
Flask==1.0.2
bitarray==0.9.2
simple-crypt==4.1.7
